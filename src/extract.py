import requests


def callAPI(location):
    url = "https://community-open-weather-map.p.rapidapi.com/weather"

    querystring = {"q": location, "lang": "en", "units": "imperial"}

    headers = {
        "X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com",
        "X-RapidAPI-Key": "334d29e364mshb21cc145ada1879p18f9bfjsnfe9bfa11233c"
    }
    try:
        response = requests.request(
            "GET", url,
            headers=headers,
            params=querystring)
    except:
        raise Exception(f'The API OpenWeatherMap request went wrong')

    if response.status_code != 200:
        raise Exception(f'Something in the OpenWeatherMap\
            request went wrong: {response.status_code}')

    data = response.json()
    return data


attributes = ['coord', 'weather', 'main', 'wind', 'clouds',
              'rain', 'snow', 'dt', 'sys', 'timezone', 'id', 'name']
main_attribute = ['temp', 'feels_like',
                  'temp_min', 'temp_max', 'pressure', 'humidity', 'grnd_level', 'sea_level']


def integrityData(data):
    # keep main attribute not null
    for attribute in attributes:
        if attribute not in data:
            data[attribute] = None

    for attribute in main_attribute:
        if attribute not in data['main']:
            data['main'][attribute] = -1

    # keep rain data not null
    if not data['rain'] is None:
        for attribute in ['1h', '3h']:
            if attribute not in data['rain']:
                data['rain'][attribute] = 0
    else:
        data['rain'] = {
            '1h': 0, '3h': 0
        }

    # keep snow data not null
    if not data['snow'] is None:
        for attribute in ['1h', '3h']:
            if attribute not in data['snow']:
                data['snow'][attribute] = 0
    else:
        data['snow'] = {
            '1h': 0, '3h': 0
        }

    # keep wind data not null
    if not data['wind'] is None:
        for attribute in ['speed', 'deg', 'gust']:
            if attribute not in data['wind']:
                data['wind'][attribute] = -1
    else:
        data['wind'] = {
            'speed': -1, 'deg': -1, 'gust': -1
        }

    if data['clouds'] is None:
        data['clouds']['all'] = -1

    if 'visibility' not in data:
        data['visibility'] = -1
    return data
