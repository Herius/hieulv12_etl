import transform as tf
from initDB import connectToDB


def loadFact(data, weather_ID, timeStamp):
    connection = connectToDB()
    city_ID = data['id']
    temp_current = data['main']['temp']
    temp_feel_like = data['main']['feels_like']
    temp_min = data['main']['temp_min']
    temp_max = data['main']['temp_max']
    temp_avg = tf.calculateTempAverage(temp_max, temp_min)
    temp_variation = tf.calculateTempVariation(temp_max, temp_min)
    pressure = data['main']['pressure']
    humidity = data['main']['humidity']
    visibility = data['visibility']
    wind_speed = data['wind']['speed']
    wind_deg = data['wind']['deg']
    win_gust = data['wind']['gust']
    rain_1h = data['rain']['1h']
    rain_3h = data['rain']['3h']
    snow_1h = data['snow']['1h']
    snow_3h = data['snow']['3h']
    sea_level = data['main']['sea_level']
    grnd_level = data['main']['grnd_level']
    sunrise_Time = tf.secondFromZeroHour(data['sys']['sunrise'])
    sunset_Time = tf.secondFromZeroHour(data['sys']['sunset'])

    hahaha = (city_ID, weather_ID, timeStamp, temp_current, temp_feel_like, temp_min,
              temp_max, temp_avg, temp_variation, pressure, humidity, visibility, wind_speed, wind_deg,
              win_gust, rain_1h, rain_3h, snow_1h, snow_3h, sea_level, grnd_level, sunrise_Time, sunset_Time)
    mySql_insert_query = """INSERT INTO `fact_weather` VALUES 
        (NULL, %s, %s, %s, %s, %s, %s, %s, %s,
        %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);;"""

    try:
        cursor = connection.cursor()
        cursor.execute(mySql_insert_query, hahaha)
        connection.commit()

    except:
        raise Exception(f'The API OpenWeatherMap request went wrong')

    finally:
        if connection.is_connected():
            connection.close()
            cursor.close()
