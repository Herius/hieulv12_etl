import mysql.connector
from mysql.connector import Error

# connect to db


def connectToDB():
    connection = mysql.connector.connect(host='localhost',
                                         user='root',
                                         database='hieulv12_etldb',
                                         password='')
    return connection

# create DB


def createDB():
    try:
        connection = mysql.connector.connect(host='localhost',
                                             user='root',
                                             password='')

        cursor = connection.cursor()
        cursor.execute("CREATE DATABASE hieulv12_etldb")
        cursor.close()
        connection.close()
        print("Database created successfully ")

    except mysql.connector.Error as error:
        print("Failed to create database in MySQL: {}".format(error))

# create table and add constraint


def createTable():
    try:
        connection = connectToDB()

        mySql_Create_Table_Query = """CREATE TABLE `dim_location` (
                                    `City_ID` int(11) NOT NULL,
                                    `City_Name` varchar(20) NOT NULL,
                                    `City_Latitude` float NOT NULL,
                                    `City_Lonitude` float NOT NULL,
                                    `Country` varchar(20) NOT NULL,
                                    `Time_Zone` tinyint(4) NOT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; """

        cursor = connection.cursor()
        cursor.execute(mySql_Create_Table_Query)

        mySql_Create_Table_Query = """CREATE TABLE `dim_time` (
                                    `Time_Stamp` timestamp NOT NULL,
                                    `Date_Local` datetime NOT NULL,
                                    `Time_Of_Day` time NOT NULL,
                                    `Day` tinyint(4) NOT NULL,
                                    `Month` tinyint(4) NOT NULL,
                                    `Quater` tinyint(4) NOT NULL,
                                    `Year` smallint(4) NOT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;; """

        cursor.execute(mySql_Create_Table_Query)

        mySql_Create_Table_Query = """CREATE TABLE `dim_weather_condition` (
                                    `Weather_ID` int(11) NOT NULL,
                                    `Weather_Main` varchar(16) NOT NULL,
                                    `Weather_Description` varchar(50) NOT NULL,
                                    `Weather_Icon` varchar(3) NOT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                                    """

        cursor.execute(mySql_Create_Table_Query)

        mySql_Create_Table_Query = """CREATE TABLE `fact_weather` (
                                    `Id` int(11) NOT NULL,
                                    `City_ID` int(11) NOT NULL,
                                    `Weather_ID` int(11) NOT NULL,
                                    `Time_Stamp` timestamp NOT NULL,
                                    `Temp_Current` float NOT NULL,
                                    `Temp_feel_like` float NOT NULL,
                                    `Temp_min` float NOT NULL,
                                    `Temp_max` float NOT NULL,
                                    `Temp_avg` float NOT NULL,
                                    `Temp_variation` float NOT NULL,
                                    `Pressure` float NOT NULL,
                                    `Humidity` float NOT NULL,
                                    `Visibility` float NOT NULL,
                                    `Wind_speed` float NOT NULL,
                                    `Win_deg` float NOT NULL,
                                    `Win_gust` float NOT NULL,
                                    `rain_1h` float NOT NULL,
                                    `rain_3h` float NOT NULL,
                                    `snow_1h` float NOT NULL,
                                    `snow_3h` float NOT NULL,
                                    `sea_level` float NOT NULL,
                                    `grnd_level` float NOT NULL,
                                    `Sunrise_Time` int(11) NOT NULL,
                                    `Sunset_Time` int(11) NOT NULL
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                                    """

        cursor.execute(mySql_Create_Table_Query)

        mySql_Add_Contrain_Query = """ALTER TABLE `dim_location`
                                    ADD PRIMARY KEY (`City_ID`);
                                    """
        cursor.execute(mySql_Add_Contrain_Query)

        mySql_Add_Contrain_Query = """ALTER TABLE `dim_time`
                                    ADD PRIMARY KEY (`Time_Stamp`);
                                    """
        cursor.execute(mySql_Add_Contrain_Query)

        mySql_Add_Contrain_Query = """ALTER TABLE `dim_weather_condition`
                                    ADD PRIMARY KEY (`Weather_ID`);
                                    """
        cursor.execute(mySql_Add_Contrain_Query)

        mySql_Add_Contrain_Query = """ALTER TABLE `fact_weather`
                                    ADD PRIMARY KEY (`Id`);
                                    """
        cursor.execute(mySql_Add_Contrain_Query)

        mySql_Add_Contrain_Query = """ALTER TABLE `fact_weather`
                                    MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
                                    """
        cursor.execute(mySql_Add_Contrain_Query)

        mySql_Add_Contrain_Query = """ALTER TABLE `fact_weather`
                                    ADD CONSTRAINT `fk_fact_location` FOREIGN KEY (`City_ID`) REFERENCES `dim_location` (`City_ID`) ,
                                    ADD CONSTRAINT `fk_fact_time` FOREIGN KEY (`Time_Stamp`) REFERENCES `dim_time` (`Time_Stamp`),
                                    ADD CONSTRAINT `fk_fact_weather_condition` FOREIGN KEY (`Weather_ID`) REFERENCES `dim_weather_condition` (`Weather_ID`);
                                    """
        cursor.execute(mySql_Add_Contrain_Query)
        print("Initially Created successfully ")
    except mysql.connector.Error as error:
        print("Failed to create table in MySQL: {}".format(error))
    finally:
        if connection.is_connected():
            connection.close()
            cursor.close()

