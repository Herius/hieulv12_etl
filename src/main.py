import __init__ as i
import load_dims as ld
import load_fact as lf
import extract as e
import time
import schedule

import datetime

cityList = ['London,uk', 'Hanoi,vn', 'Los Angeles,us', 'Roma,it', "tokyo,jp"]


def runSchedule():
    for city in cityList:
        time.sleep(3)
        currentTimeStamp = datetime.datetime.now()
        data = e.callAPI(city)
        data = e.integrityData(data)
        if not data is None:
            ld.loadDimTime(data, currentTimeStamp)
            ld.loadDimLocation(data)
            ld.loadDimWeatherCondition(data)
            for line in data['weather']:
                lf.loadFact(data, line['id'], currentTimeStamp)


# schedule to run every 30 seconds
schedule.every(30).seconds.do(runSchedule)
while True:
    schedule.run_pending()
    time.sleep(1)
