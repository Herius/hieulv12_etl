import transform as tf
from initDB import connectToDB


def loadDimTime(data, timestamp):
    connection = connectToDB()
    queryVariables = tf.tranformTime(timestamp,data)
    mySql_insert_query = """INSERT INTO `dim_time` 
    (`Time_Stamp`, `Date_Local`, `Time_Of_Day`, `Day`, `Month`, `Quater`, `Year`) 
    VALUES (%s, %s, %s, %s, %s, %s, %s);"""

    try:
        cursor = connection.cursor()
        cursor.execute(mySql_insert_query, queryVariables)
        connection.commit()

    except:
        raise Exception(f'The request to insert went wrong')

    finally:
        if connection.is_connected():
            connection.close()
            cursor.close()


def loadDimLocation(data):
    connection = connectToDB()
    city_ID = data['id']
    city_Name = data['name']
    city_lattitude = data['coord']['lat']
    city_lontitude = data['coord']['lon']
    country = data['sys']['country']
    time_Zone = data['timezone'] / 3600

    queryVariables = (city_ID, city_Name, city_lattitude,
                      city_lontitude, country, time_Zone)
    mySql_insert_query = """INSERT IGNORE INTO `dim_location` 
                        (`City_ID`, `City_Name`,
                        `City_Latitude`, `City_Lonitude`,
                        `Country`, `Time_Zone`)
                        VALUES (%s, %s, %s, %s, %s, %s);"""
    try:
        cursor = connection.cursor()
        cursor.execute(mySql_insert_query, queryVariables)
        connection.commit()
    except:
        raise Exception(f'The Insert request went wrong')
    finally:
        if connection.is_connected():
            connection.close()
            cursor.close()


def loadDimWeatherCondition(data):
    connection = connectToDB()
    for element in data['weather']:
        weather_ID = element['id']
        weather_Main = element['main']
        weather_Description = element['description']
        weather_Icon = element['icon']
        queryVariables = (weather_ID, weather_Main,
                          weather_Description, weather_Icon)
        mySql_insert_query = """
                            INSERT IGNORE  INTO `dim_weather_condition` 
                            (`Weather_ID`,
                            `Weather_Main`,
                            `Weather_Description`,
                            `Weather_Icon`) 
                            VALUES (%s, %s, %s, %s);"""
    try:
        cursor = connection.cursor()
        cursor.execute(mySql_insert_query, queryVariables)
        connection.commit()
    except:
        raise Exception(f'The Insert request went wrong')
    finally:
        if connection.is_connected():
            connection.close()
            cursor.close()
