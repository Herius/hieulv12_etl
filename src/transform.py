import datetime


def tranformTime(timestamp, data):
    timeLocal = datetime.datetime.fromtimestamp(
        data['dt']+data['timezone']+25200)        
    year = timeLocal.year
    month = timeLocal.month
    day = timeLocal.day
    quater = month//3
    if (month % 3 != 0):
        quater += 1
    return (timestamp, timeLocal, timeLocal, day, month, quater, year)

def calculateTempAverage(temp_max, temp_min):
    return (temp_max + temp_min) / 2

def calculateTempVariation(temp_max, temp_min):
    return abs(temp_max - temp_min)

def secondFromZeroHour(time):
    return time % 86400
