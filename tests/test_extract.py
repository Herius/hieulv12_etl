import pytest
import materials as m
from src import extract as ex

@pytest.mark.parametrize(
    "data, expectData",
    [
        (m.dataRaw1, m.dataIntegrity1),
        (m.dataRaw2, m.dataIntegrity2)
    ]
)

def test_integrityData(data, expectData):
    assert ex.integrityData(data) == expectData