import pytest
import materials as m
from src import transform as tf


@pytest.mark.parametrize(
    "temp_max, temp_min, temp_avg",
    [
        (20,10,15),
        (20,-10,5),
        (-10,-20,-15)
    ]
)

def test_calculateTempAverage(temp_max, temp_min, temp_avg):
    assert tf.calculateTempAverage(temp_max,temp_min) == temp_avg

@pytest.mark.parametrize(
    "temp_max, temp_min, temp_variation",
    [
        (20,10,10),
        (20,-10,30),
        (-10,-20,10)
    ]
)

def test_calculateTempVariation(temp_max, temp_min, temp_variation):
    assert tf.calculateTempVariation(temp_max,temp_min) == temp_variation

@pytest.mark.parametrize(
    "secondFromEpoch, secondFromZeroHuor",
    [
        (1654065783 ,24183),
        (1654099783 ,58183),
        (1659099783 ,46983)
    ]
)

def test_secondFromZeroHour(secondFromEpoch,secondFromZeroHuor):
    assert tf.secondFromZeroHour(secondFromEpoch) == secondFromZeroHuor

@pytest.mark.parametrize(
    "timeStamp, data, dateConvertTuple",
    [
        (m.timeStamp1, m.dataRaw1, m.outputDateConvert1),
        (m.timeStamp2, m.dataRaw2, m.outputDateConvert2),
    ]
)

def test_tranformTime(timeStamp, data, dateConvertTuple):
    assert tf.tranformTime(timeStamp, data) == dateConvertTuple
