import datetime
dataRaw1 = {'coord': {'lon': 105.8412, 'lat': 21.0245}, 'weather': [{'id': 804, 'main': 'Clouds', 'description': 'overcast clouds', 'icon': '04d'}], 'base': 'stations', 'main': {'temp': 93.2, 'feels_like': 102.99, 'temp_min': 93.2, 'temp_max': 93.2, 'pressure': 999, 'humidity': 53, 'sea_level': 999, 'grnd_level': 998}, 'visibility': 10000, 'wind': {'speed': 4.23, 'deg': 171, 'gust': 8.63}, 'clouds': {'all': 100}, 'dt': 1654069547, 'sys': {'type': 1, 'id': 9308, 'country': 'VN', 'sunrise': 1654035296, 'sunset': 1654083253}, 'timezone': 25200, 'id': 1581130, 'name':
            'Hanoi', 'cod': 200}
dataIntegrity1 = {'coord': {'lon': 105.8412, 'lat': 21.0245}, 'weather': [{'id': 804, 'main': 'Clouds', 'description': 'overcast clouds', 'icon': '04d'}], 'base': 'stations', 'main': {'temp': 93.2, 'feels_like': 102.99, 'temp_min': 93.2, 'temp_max': 93.2, 'pressure': 999, 'humidity': 53, 'sea_level': 999, 'grnd_level': 998}, 'visibility': 10000, 'wind': {'speed': 4.23, 'deg': 171, 'gust': 8.63}, 'clouds': {'all': 100}, 'dt': 1654069547, 'sys': {'type': 1, 'id': 9308, 'country': 'VN', 'sunrise': 1654035296, 'sunset': 1654083253}, 'timezone': 25200, 'id': 1581130, 'name':
                  'Hanoi', 'cod': 200, 'rain': {'1h': 0, '3h': 0}, 'snow': {'1h': 0, '3h': 0}}
timeStamp1 = datetime.datetime(2022, 6, 1, 14, 45, 45, 686011)
outputDateConvert1 = (datetime.datetime(2022, 6, 1, 14, 45, 45, 686011), datetime.datetime(
    2022, 6, 1, 21, 45, 47), datetime.datetime(2022, 6, 1, 21, 45, 47), 1, 6, 2, 2022)

dataRaw2 = {'coord': {'lon': 139.6917, 'lat': 35.6895}, 'weather': [{'id': 803, 'main': 'Clouds', 'description': 'broken clouds', 'icon': '04d'}], 'base': 'stations', 'main': {'temp': 75.54, 'feels_like': 74.86, 'temp_min': 72.66, 'temp_max': 77.59, 'pressure': 1007, 'humidity': 44}, 'visibility': 10000, 'wind': {
    'speed': 14.97, 'deg': 190}, 'clouds': {'all': 75}, 'dt': 1654073469, 'sys': {'type': 2, 'id': 2038398, 'country': 'JP', 'sunrise': 1654025224, 'sunset': 1654077075}, 'timezone': 32400, 'id': 1850144, 'name': 'Tokyo', 'cod': 200}
dataIntegrity2 = {'coord': {'lon': 139.6917, 'lat': 35.6895}, 'weather': [{'id': 803, 'main': 'Clouds', 'description': 'broken clouds', 'icon': '04d'}], 'base': 'stations', 'main': {'temp': 75.54, 'feels_like': 74.86, 'temp_min': 72.66, 'temp_max': 77.59, 'pressure': 1007, 'humidity': 44, 'grnd_level': -1, 'sea_level': -1}, 'visibility': 10000, 'wind': {'speed': 14.97, 'deg': 190, 'gust': -1}, 'clouds': {'all': 75}, 'dt': 1654073469, 'sys': {'type': 2, 'id': 2038398, 'country': 'JP', 'sunrise': 1654025224, 'sunset': 1654077075}, 'timezone': 32400, 'id': 1850144, 'name':
                  'Tokyo', 'cod': 200, 'rain': {'1h': 0, '3h': 0}, 'snow': {'1h': 0, '3h': 0}}
timeStamp2 = datetime.datetime(2022, 6, 1, 15, 52, 0, 304731)
outputDateConvert2 = (datetime.datetime(2022, 6, 1, 15, 52, 0, 304731), datetime.datetime(
    2022, 6, 2, 0, 51, 9), datetime.datetime(2022, 6, 2, 0, 51, 9), 2, 6, 2, 2022)
